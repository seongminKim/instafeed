<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-11
 * Time: 오후 5:11
 */

namespace Eguana\InstaFeed\Test\Unit\Block;

use Eguana\InstaFeed\Model\InstaFeedRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class LoadInstaFeedDataTest
 * @package Eguana\InstaFeed\Test\Unit\Block
 */
class LoadInstaFeedDataTest
{
    /**
     * @var InstaFeedRepository
     */
    private $instaFeedRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * LoadInstaFeedDataTest constructor.
     * @param InstaFeedRepository $instaFeedRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        InstaFeedRepository $instaFeedRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->instaFeedRepository = $instaFeedRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Get Instagram data from eguana_instafeed table
     * test status => 2
     * @return array|null
     */
    public function getInstaFeed()
    {
        $filter = $this->searchCriteriaBuilder->addFilter('status', 2, 'eq')->create();
        $list = $this->instaFeedRepository->getList($filter);
        $result = [];

        if ($list->getTotalCount() > 0) {
            foreach ($list->getItems() as $key => $data) {
                $result[$key]['type'] = $data['type'];
                $result[$key]['link'] = $data['link'];
                $result[$key]['url'] = $data['url'];
            }

            return $result;
        } else {
            return null;
        }
    }
}
