<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-11
 * Time: 오후 4:57
 */

namespace Eguana\InstaFeed\Test\Unit\Cron;

use Eguana\InstaFeed\Cron\InstaFeed;
use Eguana\InstaFeed\Helper\Data;

/**
 * Class GetInstaFeedDataTest
 * @package Eguana\InstaFeed\Test\Unit\Cron
 */
class GetInstaFeedDataTest
{
    /**
     * @var InstaFeed
     */
    private $instaFeedCron;
    /**
     * @var Data
     */
    private $helper;

    /**
     * GetInstaFeedDataTest constructor.
     * @param InstaFeed $instaFeedCron
     * @param Data $helper
     */
    public function __construct(
        InstaFeed $instaFeedCron,
        Data $helper
    ) {
        $this->instaFeedCron = $instaFeedCron;
        $this->helper = $helper;
    }

    public function testGetInstaFeedData()
    {
        $client_id      = $this->helper->getConfig(Data::XML_PATH_CLIENT_ID);
        $client_secret  = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_CLIENT_SECRET));
        $redirect_uri   = $this->helper->getConfig(Data::XML_PATH_REDIRECT_URI);
        $code           = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_CODE));

        if ($this->helper->getConfig(Data::XML_PATH_TOKEN) === null) {
            $accessToken = $this->instaFeedCron->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
        } else {
            $accessToken = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_TOKEN));
        }

        $data = $this->instaFeedCron->getInstaFeed($accessToken);

        if ($data !== null) {
            return $this;
        } else {
            return null;
        }
    }
}
