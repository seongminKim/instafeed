<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-11
 * Time: 오후 5:19
 */

namespace Eguana\InstaFeed\Test\Unit\Cron;

use Eguana\InstaFeed\Model\InstaFeedFactory;
use Eguana\InstaFeed\Api\InstaFeedRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class DeleteInstaFeedDataTest
 * @package Eguana\InstaFeed\Test\Unit\Cron
 */
class DeleteInstaFeedDataTest
{
    /**
     * @var InstaFeedFactory
     */
    private $instaFeedFactory;
    /**
     * @var InstaFeedRepositoryInterface
     */
    private $instaFeedRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * DeleteInstaFeedDataTest constructor.
     * @param InstaFeedFactory $instaFeedFactory
     * @param InstaFeedRepositoryInterface $instaFeedRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        InstaFeedFactory $instaFeedFactory,
        InstaFeedRepositoryInterface $instaFeedRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->instaFeedFactory = $instaFeedFactory;
        $this->instaFeedRepository = $instaFeedRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * Delete instagram feed data from eguana_instafeed
     * test status => 2
     */
    public function deleteInstaFeedDataTest()
    {
        $filter = $this->searchCriteriaBuilder->addFilter('status', 2, 'eq')->create();
        $list = $this->instaFeedRepository->getList($filter);

        if ($list->getTotalCount() > 0) {
            foreach ($list->getItems() as $item) {
                $this->instaFeedRepository->delete($item);
            }
        }
    }
}
