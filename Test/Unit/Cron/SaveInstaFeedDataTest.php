<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-11
 * Time: 오후 5:08
 */

namespace Eguana\InstaFeed\Test\Unit\Cron;

use Eguana\InstaFeed\Model\InstaFeedFactory;
use Eguana\InstaFeed\Api\InstaFeedRepositoryInterface;
use Eguana\InstaFeed\Cron\InstaFeed;
use Eguana\InstaFeed\Helper\Data;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class SaveInstaFeedDataTest
 * @package Eguana\InstaFeed\Test\Unit\Cron
 */
class SaveInstaFeedDataTest
{
    /**
     * @var InstaFeed
     */
    private $instaFeedCron;
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var GetInstaFeedDataTest
     */
    private $instaFeedDataTest;
    /**
     * @var InstaFeedFactory
     */
    private $instaFeedFactory;
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var InstaFeedRepositoryInterface
     */
    private $instaFeedRepository;

    /**
     * GetInstaFeedDataTest constructor.
     * @param InstaFeed $instaFeedCron
     * @param Data $helper
     * @param GetInstaFeedDataTest $instaFeedDataTest
     * @param InstaFeedFactory $instaFeedFactory
     * @param DateTime $dateTime
     * @param InstaFeedRepositoryInterface $instaFeedRepository
     */
    public function __construct(
        InstaFeed $instaFeedCron,
        Data $helper,
        GetInstaFeedDataTest $instaFeedDataTest,
        InstaFeedFactory $instaFeedFactory,
        DateTime $dateTime,
        InstaFeedRepositoryInterface $instaFeedRepository
    ) {
        $this->instaFeedCron = $instaFeedCron;
        $this->helper = $helper;
        $this->instaFeedDataTest = $instaFeedDataTest;
        $this->instaFeedFactory = $instaFeedFactory;
        $this->dateTime = $dateTime;
        $this->instaFeedRepository = $instaFeedRepository;
    }

    /**
     * Test save instagram feed data to eguana_instafeed
     * test status => 2
     */
    public function saveInstaFeedDataTest()
    {
        $data = $this->instaFeedDataTest->testGetInstaFeedData();

        foreach ($data as $key => $item) {
            $instaFeedData = [
                'type' => $item['type'],
                'link' => $item['link'],
                'status' => 2,
                'created_at' => $this->dateTime->gmtDate()
            ];

            if ($item['type'] === 'carousel') {
                if ($item['carousel_media'][0]['type'] === 'image') {
                    $instaFeedData['type'] = $item['type'] . "_" . $item['carousel_media'][0]['type'];
                    $instaFeedData['url'] = $item['carousel_media'][0]['images']['standard_resolution']['url'];
                } elseif ($item['carousel_media'][0]['type'] === 'video') {
                    $instaFeedData['type'] = $item['type'] . "_" . $item['carousel_media'][0]['type'];
                    $instaFeedData['url'] = $item['carousel_media'][0]['images']['standard_resolution']['url'];
                }

                $instaFeedModel = $this->instaFeedFactory->create();
                $this->instaFeedRepository->save($instaFeedModel->addData($instaFeedData));
            } elseif ($item['type'] !== 'carousel') {
                if ($item['type'] === 'image') {
                    $instaFeedData['url'] = $item['images']['standard_resolution']['url'];
                } elseif ($item['type'] === 'video') {
                    $instaFeedData['url'] = $item['images']['standard_resolution']['url'];
                }

                $instaFeedModel = $this->instaFeedFactory->create();
                $this->instaFeedRepository->save($instaFeedModel->addData($instaFeedData));
            }
        }
    }
}
