Eguana_InstaFeed v1.0.0

`Website` : Main Website (COSRX)  
`Author` : Glenn  
`DB Table Name` : eguana_instafeed  

***Explanation***  

>1. Change `Enable` value to `Yes`.
>
>1. Enter `Client ID` and `Client Secret`.
>
>1. Click `Get Code` button.
>
>1. Sign in and Allow permissions.
>
>1. Copy the code from the new tab and paste it into the `Code` field.
>
>1. Enter the number of images to be displayed on the screen in the `Count` field.
>
>1. Enter the time to run in the `Cron Schedule` field in crontab format.
>
>1. Click `Save Config`.