<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-07
 * Time: 오후 7:32
 */

namespace Eguana\InstaFeed\Block\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Config\Block\System\Config\Form\Field;

class Button extends Field
{
    protected $_template = 'Eguana_InstaFeed::system/config/button.phtml';

    public function render(AbstractElement $element)
    {
        $element->unsScope();

        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $originalData = $element->getOriginalData();
        $this->addData([
            'button_label'  => $originalData['button_label'],
            'button_url'    => $this->getUrl($originalData['button_url'], ['_current' => true]),
            'html_id'       => $element->getHtmlId(),
        ]);

        return $this->_toHtml();
    }
}
