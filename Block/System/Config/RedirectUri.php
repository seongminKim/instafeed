<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-07
 * Time: 오후 7:07
 */

namespace Eguana\InstaFeed\Block\System\Config;


use Eguana\InstaFeed\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class RedirectUri extends Field
{
    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        Context $context,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        $routeId = Data::ROUTE_ID;
        $redirectUri = $baseUrl . $routeId;

        $element->setReadonly(false);
        $element->setData('value', $redirectUri);

        return parent::_getElementHtml($element);
    }
}