<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-27
 * Time: 오후 3:33
 */

namespace Eguana\InstaFeed\Block;

use Eguana\InstaFeed\Model\InstaFeedRepository;
use Eguana\InstaFeed\Helper\Data;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;

/**
 * Class InstaFeed
 * @package Eguana\InstaFeed\Block
 */
class InstaFeed extends Template
{
    /**
     * @var InstaFeedRepository
     */
    private $instaFeedRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var Data
     */
    private $helper;

    /**
     * InstaFeed constructor.
     * @param Template\Context $context
     * @param InstaFeedRepository $instaFeedRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        InstaFeedRepository $instaFeedRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->instaFeedRepository = $instaFeedRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->helper = $helper;
    }

    /**
     * Get Instagram data from eguana_instafeed table
     * @return array|null
     */
    public function getInstaFeed()
    {
        $filter = $this->searchCriteriaBuilder->addFilter('status', 1, 'eq')->create();
        $list = $this->instaFeedRepository->getList($filter);
        $result = [];

        if ($list->getTotalCount() > 0) {
            foreach ($list->getItems() as $key => $data) {
                $result[$key]['type'] = $data['type'];
                $result[$key]['link'] = $data['link'];
                $result[$key]['url'] = $data['url'];
            }

            return $result;
        } else {
            return null;
        }
    }

    /**
     * Get Instagram Feed Enable
     * @return mixed
     */
    public function getInstaFeedEnable()
    {
        return $this->helper->getConfig(Data::XML_PATH_ACTIVE);
    }
}
