<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-29
 * Time: 오후 3:46
 */

namespace Eguana\InstaFeed\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class InstaFeed
 * @package Eguana\InstaFeed\Model\ResourceModel
 */
class InstaFeed extends AbstractDb
{
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('eguana_instafeed', 'entity_id');
    }
}
