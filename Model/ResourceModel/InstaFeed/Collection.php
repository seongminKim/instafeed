<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-29
 * Time: 오후 3:46
 */

namespace Eguana\InstaFeed\Model\ResourceModel\InstaFeed;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Eguana\InstaFeed\Model\ResourceModel\InstaFeed
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('Eguana\InstaFeed\Model\InstaFeed', 'Eguana\InstaFeed\Model\ResourceModel\InstaFeed');
    }
}
