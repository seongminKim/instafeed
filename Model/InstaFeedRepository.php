<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-05
 * Time: 오후 3:13
 */

namespace Eguana\InstaFeed\Model;

use Eguana\InstaFeed\Api\Data\InstaFeedInterface;
use Eguana\InstaFeed\Api\InstaFeedRepositoryInterface;
use Eguana\InstaFeed\Model\ResourceModel\InstaFeed\CollectionFactory;
use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class InstaFeedRepository
 * @package Eguana\InstaFeed\Model
 */
class InstaFeedRepository implements InstaFeedRepositoryInterface
{
    /**
     * @var InstaFeedFactory
     */
    private $instaFeedFactory;
    /**
     * @var ResourceModel\InstaFeed
     */
    private $instaFeedResourceModel;
    /**
     * @var ResourceModel\InstaFeed\CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsInterfaceFactory;

    /**
     * InstaFeedRepository constructor.
     * @param InstaFeedFactory $instaFeedFactory
     * @param ResourceModel\InstaFeed $instaFeedResourceModel
     * @param ResourceModel\InstaFeed\CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsInterfaceFactory
     */
    public function __construct(
        InstaFeedFactory $instaFeedFactory,
        \Eguana\InstaFeed\Model\ResourceModel\InstaFeed $instaFeedResourceModel,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsInterfaceFactory
    ) {
        $this->instaFeedFactory = $instaFeedFactory;
        $this->instaFeedResourceModel = $instaFeedResourceModel;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsInterfaceFactory = $searchResultsInterfaceFactory;
    }

    /**
     * @param InstaFeedInterface $instaFeed
     * @return InstaFeedInterface
     * @throws CouldNotSaveException
     */
    public function save(InstaFeedInterface $instaFeed)
    {
        try {
            $this->instaFeedResourceModel->save($instaFeed);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }

        return $instaFeed;
    }

    /**
     * @param InstaFeedInterface $instaFeed
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(InstaFeedInterface $instaFeed)
    {
        try {
            $this->instaFeedResourceModel->delete($instaFeed);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }

        return true;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsInterfaceFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];

            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }

            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }

        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();

        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        $instaFeed = [];

        foreach ($collection as $item) {
            $instaFeed[] = $item;
        }

        $searchResults->setItems($instaFeed);

        return $searchResults;
    }

    /**
     * @param $entityId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($entityId)
    {
        $instaFeed = $this->instaFeedFactory->create();
        $this->instaFeedResourceModel->load($instaFeed, $entityId);

        if (!$instaFeed->getId()) {
            throw new NoSuchEntityException(__('instagram feed data with id "%1" does not exist.', $entityId));
        }

        return $instaFeed;
    }

    /**
     * @param $entityId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($entityId)
    {
        return $this->delete($this->getById($entityId));
    }
}
