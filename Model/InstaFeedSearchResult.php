<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-05
 * Time: 오후 3:31
 */

namespace Eguana\InstaFeed\Model;

use Eguana\InstaFeed\Api\Data\InstaFeedSearchResultInterface;
use Magento\Framework\Api\Search\SearchResult;

/**
 * Class InstaFeedSearchResult
 * @package Eguana\InstaFeed\Model
 */
class InstaFeedSearchResult extends SearchResult implements InstaFeedSearchResultInterface
{

}
