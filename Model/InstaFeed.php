<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-29
 * Time: 오후 3:46
 */

namespace Eguana\InstaFeed\Model;

use Eguana\InstaFeed\Api\Data\InstaFeedInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class InstaFeed
 * @package Eguana\InstaFeed\Model
 */
class InstaFeed extends AbstractModel implements InstaFeedInterface
{
    protected function _construct()
    {
        $this->_init('Eguana\InstaFeed\Model\ResourceModel\InstaFeed');
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->_getData('entity_id');
    }

    /**
     * @param int $entityId
     * @return InstaFeed|AbstractModel|mixed
     */
    public function setEntityId($entityId)
    {
        return $this->setData('entity_id', $entityId);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->_getData('type');
    }

    /**
     * @param $type
     * @return InstaFeed|mixed
     */
    public function setType($type)
    {
        return $this->setData('type', $type);
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->_getData('link');
    }

    /**
     * @param $link
     * @return InstaFeed|mixed
     */
    public function setLink($link)
    {
        return $this->setData('link', $link);
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->_getData('url');
    }

    /**
     * @param $url
     * @return InstaFeed|mixed
     */
    public function setUrl($url)
    {
        return $this->setData('url', $url);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->_getData('status');
    }

    /**
     * @param $status
     * @return InstaFeed|mixed
     */
    public function setStatus($status)
    {
        return $this->setData('status', $status);
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->_getData('created_at');
    }

    /**
     * @param $createdAt
     * @return InstaFeed|mixed
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData('created_at', $createdAt);
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->_getData('updated_at');
    }

    /**
     * @param $updatedAt
     * @return InstaFeed|mixed
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData('updated_at');
    }
}
