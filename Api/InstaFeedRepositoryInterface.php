<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-05
 * Time: 오후 3:09
 */

namespace Eguana\InstaFeed\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Eguana\InstaFeed\Api\Data\InstaFeedInterface;

/**
 * Interface InstaFeedRepositoryInterface
 * @package Eguana\InstaFeed\Api
 */
interface InstaFeedRepositoryInterface
{
    /**
     * @param InstaFeedInterface $instaFeed
     * @return InstaFeedInterface
     */
    public function save(InstaFeedInterface $instaFeed);

    /**
     * @param InstaFeedInterface $instaFeed
     * @return bool
     */
    public function delete(InstaFeedInterface $instaFeed);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param $entityId
     * @return mixed
     */
    public function getById($entityId);

    /**
     * @param $entityId
     * @return bool
     */
    public function deleteById($entityId);
}
