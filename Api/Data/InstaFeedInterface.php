<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-05
 * Time: 오후 2:54
 */

namespace Eguana\InstaFeed\Api\Data;

/**
 * Interface InstaFeedInterface
 * @package Eguana\InstaFeed\Api\Data
 */
interface InstaFeedInterface
{
    /**
     * @return mixed
     */
    public function getEntityId();

    /**
     * @param $entityId
     * @return mixed
     */
    public function setEntityId($entityId);

    /**
     * @return mixed
     */
    public function getType();

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type);

    /**
     * @return mixed
     */
    public function getLink();

    /**
     * @param $link
     * @return mixed
     */
    public function setLink($link);

    /**
     * @return mixed
     */
    public function getUrl();

    /**
     * @param $url
     * @return mixed
     */
    public function setUrl($url);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $createdAt
     * @return mixed
     */
    public function setCreatedAt($createdAt);

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param $updatedAt
     * @return mixed
     */
    public function setUpdatedAt($updatedAt);
}
