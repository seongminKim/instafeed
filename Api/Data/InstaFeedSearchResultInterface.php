<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-06-05
 * Time: 오후 3:26
 */

namespace Eguana\InstaFeed\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface InstaFeedSearchResultInterface
 * @package Eguana\InstaFeed\Api\Data
 */
interface InstaFeedSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items);
}
