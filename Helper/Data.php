<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-22
 * Time: 오후 4:46
 */

namespace Eguana\InstaFeed\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

/**
 * Class Data
 * @package Eguana\InstaFeed\Helper
 */
class Data extends AbstractHelper
{
    const ROUTE_ID                  = 'instafeed';
    const XML_PATH_ACTIVE           = 'instagram/feed/active';
    const XML_PATH_CLIENT_ID        = 'instagram/feed/client_id';
    const XML_PATH_CLIENT_SECRET    = 'instagram/feed/client_secret';
    const XML_PATH_REDIRECT_URI     = 'instagram/feed/redirect_uri';
    const XML_PATH_CODE             = 'instagram/feed/code';
    const XML_PATH_TOKEN            = 'instagram/feed/token';
    const XML_PATH_COUNT            = 'instagram/feed/count';

    /**
     * @var EncryptorInterface
     */
    private $encryptor;
    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * Data constructor.
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param WriterInterface $configWriter
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor,
        WriterInterface $configWriter
    ) {
        parent::__construct($context);
        $this->encryptor = $encryptor;
        $this->configWriter = $configWriter;
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORES);
    }

    /**
     * @param $path
     * @param $value
     */
    public function setConfig($path, $value)
    {
        $this->configWriter->save($path, $value, $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $scopeId = 0);
    }

    /**
     * @param $data
     * @return string
     */
    public function decryptData($data)
    {
        return $this->encryptor->decrypt($data);
    }

    /**
     * @param $data
     * @return string
     */
    public function encryptData($data)
    {
        return $this->encryptor->encrypt($data);
    }
}
