<?php

/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-28
 * Time: 오전 10:52
 */

namespace Eguana\InstaFeed\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * get instagram api code controller
 * @package Eguana\InstaFeed\Controller\Index
 */
class Index extends Action
{
    /**
     * @var Http
     */
    private $request;

    /**
     * Index constructor.
     * @param Context $context
     * @param Http $request
     */
    public function __construct(
        Context $context,
        Http $request
    ) {
        parent::__construct($context);
        $this->request = $request;
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if ($this->request->getParam('code')) {
            $code = $this->request->getParam('code');

            return $this->getResponse()->setBody('Your CODE : ' . $code);
        } else {
            return $this->getResponse()->setBody('Error');
        }
    }
}
