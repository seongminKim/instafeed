<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-28
 * Time: 오후 4:22
 */

namespace Eguana\InstaFeed\Cron;

use Eguana\InstaFeed\Model\InstaFeedFactory;
use Eguana\InstaFeed\Api\InstaFeedRepositoryInterface;
use Eguana\InstaFeed\Helper\Data;
use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Filesystem\Driver\file;
use Magento\Framework\HTTP\Client\Curl;

/**
 * Class InstaFeed
 * @package Eguana\InstaFeed\Cron
 */
class InstaFeed
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ManagerInterface
     */
    private $manager;
    /**
     * @var DateTime
     */
    private $dateTime;
    /**
     * @var InstaFeedRepositoryInterface
     */
    private $instaFeedRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var file
     */
    private $driver;
    /**
     * @var InstaFeedFactory
     */
    private $instaFeedFactory;
    /**
     * @var Curl
     */
    private $curl;

    /**
     * InstaFeed constructor.
     * @param Data $helper
     * @param SerializerInterface $serializer
     * @param ManagerInterface $manager
     * @param DateTime $dateTime
     * @param InstaFeedRepositoryInterface $instaFeedRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param file $driver
     * @param InstaFeedFactory $instaFeedFactory
     * @param Curl $curl
     */
    public function __construct(
        Data $helper,
        SerializerInterface $serializer,
        ManagerInterface $manager,
        DateTime $dateTime,
        InstaFeedRepositoryInterface $instaFeedRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        file $driver,
        InstaFeedFactory $instaFeedFactory,
        Curl $curl
    ) {
        $this->helper = $helper;
        $this->serializer = $serializer;
        $this->manager = $manager;
        $this->dateTime = $dateTime;
        $this->instaFeedRepository = $instaFeedRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->driver = $driver;
        $this->instaFeedFactory = $instaFeedFactory;
        $this->curl = $curl;
    }

    /**
     * Get Instagram API access token
     * @param $client_id
     * @param $client_secret
     * @param $redirect_uri
     * @param $code
     * @return string|null
     */
    public function getAccessToken($client_id, $client_secret, $redirect_uri, $code)
    {
        try {
            $url = "https://api.instagram.com/oauth/access_token";
            $params = [
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'grant_type' => 'authorization_code',
                'redirect_uri' => $redirect_uri,
                'code' => $code
            ];

            $this->curl->post($url, $params);
            $response = $this->curl->getBody();
            $result = $this->serializer->unserialize($response);

            $this->helper->setConfig(Data::XML_PATH_TOKEN, $this->helper->encryptData($result['access_token']));

            return $result['access_token'];
        } catch (Exception $e) {
            $this->manager->addErrorMessage($e->getMessage());

            return null;
        }
    }

    /**
     * Get Instagram Feed Data
     * @param $token
     * @return array|null
     */
    public function getInstaFeed($token)
    {
        $accessToken    = $token;
        $count          = $this->helper->getConfig(Data::XML_PATH_COUNT);
        $apiUrl         = "https://api.instagram.com/v1/users/self/media/recent/?access_token=";

        if ($accessToken) {
            try {
                $source = $this->driver->fileGetContents($apiUrl . $accessToken . "&count=" . $count);
                $result = $this->serializer->unserialize($source);

                return $result['data'];
            } catch (Exception $e) {
                $this->manager->addErrorMessage($e->getMessage());

                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Save Instagram Feed Data to eguana_instafeed table
     * @param $data
     */
    public function saveInstaFeedData($data)
    {
        foreach ($data as $key => $item) {
            $instaFeedData = [
                'type' => $item['type'],
                'link' => $item['link'],
                'url' => $item['images']['standard_resolution']['url'],
                'status' => 1,
                'created_at' => $this->dateTime->gmtDate()
            ];

            $instaFeedModel = $this->instaFeedFactory->create();
            $this->instaFeedRepository->save($instaFeedModel->addData($instaFeedData));
        }
    }

    /**
     * eguana_instafeed_cron
     * @return $this
     */
    public function execute()
    {
        if ($this->helper->getConfig(Data::XML_PATH_ACTIVE)) {
            $client_id      = $this->helper->getConfig(Data::XML_PATH_CLIENT_ID);
            $client_secret  = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_CLIENT_SECRET));
            $redirect_uri   = $this->helper->getConfig(Data::XML_PATH_REDIRECT_URI);
            $code           = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_CODE));

            if ($this->helper->getConfig(Data::XML_PATH_TOKEN) === null) {
                $accessToken = $this->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
            } else {
                $accessToken = $this->helper->decryptData($this->helper->getConfig(Data::XML_PATH_TOKEN));
            }

            $filter = $this->searchCriteriaBuilder->addFilter('status', 1, 'eq')->create();
            $list = $this->instaFeedRepository->getList($filter);

            $data = $this->getInstaFeed($accessToken);

            if (isset($data[0])) {
                if ($list->getTotalCount() > 0) {
                    foreach ($list->getItems() as $item) {
                        $this->instaFeedRepository->delete($item);
                    }
                }

                $this->saveInstaFeedData($data);

                return $this;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
