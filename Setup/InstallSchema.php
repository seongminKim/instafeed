<?php
/**
 * Created by Eguana.
 * User: glenn
 * Date: 2019-05-29
 * Time: 오후 3:44
 */

namespace Eguana\InstaFeed\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

/**
 * Class InstallSchema
 * @package Eguana\InstaFeed\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     *
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = 'eguana_instafeed';
        if (!$installer->tableExists($tableName)) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable($tableName)
            )
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )
                ->addColumn(
                    'type',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Type'
                )
                ->addColumn(
                    'link',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Link'
                )
                ->addColumn(
                    'url',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Data Url'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'default' => 1],
                    'Status'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                )->addIndex(
                    $installer->getIdxName($tableName, ['entity_id']),
                    ['entity_id']
                )
                ->setComment('InstaFeed');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}